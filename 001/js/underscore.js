//     Underscore.js 1.8.3
//     http://underscorejs.org
//     (c) 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
//     Underscore may be freely distributed under the MIT license.

(function() {

  // Baseline setup
  // --------------

  // ブラウザの`window` または　サーバーの `exports`のルートオブジェクトを確立させる
  var root = this;

  // `_` 変数の前の値を保持する.
  var previousUnderscore = root._;

  //zipで圧縮されていない縮小されたバイトを保持する
  var ArrayProto = Array.prototype, ObjProto = Object.prototype, FuncProto = Function.prototype;

  //コアのプロトタイプへの高速アクセスのためのクイックリファレンス変数を作成
  var
    push             = ArrayProto.push,
    slice            = ArrayProto.slice,
    toString         = ObjProto.toString,
    hasOwnProperty   = ObjProto.hasOwnProperty;

  // 使用するすべてのECMAScript 5のネイティブ関数を実装
  // ここで宣言する
  var
    nativeIsArray      = Array.isArray,
    nativeKeys         = Object.keys,
    nativeBind         = FuncProto.bind,
    nativeCreate       = Object.create;

  //直訳：代理プロトタイプスワッピングのための裸の関数参照。
  //プロトタイプを入れ替えるための機能
  var Ctor = function(){};

  //以下に使用するためのアンダーオブジェクトへの安全な参照を作成します。
  var _ = function(obj) {
    if (obj instanceof _) return obj;
    if (!(this instanceof _)) return new _(obj);
    this._wrapped = obj;
  };

  // Node.jsのためにアンダースコアオブジェクトをエクスポート
  // 古い `必要（）` APIの下位互換性。私たちは、にしている場合
  // ブラウザは、グローバルオブジェクトとして_` `追加します。
  if (typeof exports !== 'undefined') {
    if (typeof module !== 'undefined' && module.exports) {
      exports = module.exports = _;
    }
    exports._ = _;
  } else {
    root._ = _;
  }

  // 使用バージョン
  _.VERSION = '1.8.3';

  // 効率的な（現在のエンジン用）バージョンを返す内部関数
  // 渡されたコールバックを繰り返し、他のアンダースコアに適用されます
  // 機能
  var optimizeCb = function(func, context, argCount) {
    if (context === void 0) return func;
    switch (argCount == null ? 3 : argCount) {
      case 1: return function(value) {
        return func.call(context, value);
      };
      case 2: return function(value, other) {
        return func.call(context, value, other);
      };
      case 3: return function(value, index, collection) {
        return func.call(context, value, index, collection);
      };
      case 4: return function(accumulator, value, index, collection) {
        return func.call(context, accumulator, value, index, collection);
      };
    }
    return function() {
      return func.apply(context, arguments);
    };
  };

  // 主に内部関数を適用することができるコールバックを生成します
  // コレクション内の各要素に、所望の結果を返す - のいずれか
  // identity、任意のコールバック、プロパティ・マッチャ、またはプロパティアクセサ。
  var cb = function(value, context, argCount) {
    if (value == null) return _.identity;
    if (_.isFunction(value)) return optimizeCb(value, context, argCount);
    if (_.isObject(value)) return _.matcher(value);
    return _.property(value);
  };
  _.iteratee = function(value, context) {
    return cb(value, context, Infinity);
  };

  // 割当機能を作成するための内部関数。
  var createAssigner = function(keysFunc, undefinedOnly) {
    return function(obj) {
      var length = arguments.length;
      if (length < 2 || obj == null) return obj;
      for (var index = 1; index < length; index++) {
        var source = arguments[index],
            keys = keysFunc(source),
            l = keys.length;
        for (var i = 0; i < l; i++) {
          var key = keys[i];
          if (!undefinedOnly || obj[key] === void 0) obj[key] = source[key];
        }
      }
      return obj;
    };
  };

  // 他から継承する新しいオブジェクトを作成するための内部関数。
  var baseCreate = function(prototype) {
    if (!_.isObject(prototype)) return {};
    if (nativeCreate) return nativeCreate(prototype);
    Ctor.prototype = prototype;
    var result = new Ctor;
    Ctor.prototype = null;
    return result;
  };

  var property = function(key) {
    return function(obj) {
      return obj == null ? void 0 : obj[key];
    };
  };

  // コレクションかどうかを決定する収集方法のヘルパー
  // 配列またはオブジェクトとして繰り返されるべきです
  // 関連: http://people.mozilla.org/~jorendorff/es6-draft.html#sec-tolength
  // ARM-64に非常に厄介なのiOS 8 JITのバグを回避することができます。 ＃2094
  var MAX_ARRAY_INDEX = Math.pow(2, 53) - 1;
  var getLength = property('length');
  var isArrayLike = function(collection) {
    var length = getLength(collection);
    return typeof length == 'number' && length >= 0 && length <= MAX_ARRAY_INDEX;
  };
  // 関数一覧
  // --------------------

  // 礎石, an `each` 実装, 別名 `forEach`.
  // 全ての扱い　ハンドル 生のオブジェクト 加える 配列好き
  // 彼らが密であるかのように疎な配列は、好きです。
  _.each = _.forEach = function(obj, iteratee, context) {
    iteratee = optimizeCb(iteratee, context);
    var i, length;
    if (isArrayLike(obj)) {
      for (i = 0, length = obj.length; i < length; i++) {
        iteratee(obj[i], i, obj);
      }
    } else {
      var keys = _.keys(obj);
      for (i = 0, length = keys.length; i < length; i++) {
        iteratee(obj[keys[i]], keys[i], obj);
      }
    }
    return obj;
  };

  // 各要素にiterateeを適用した結果を返します。
  _.map = _.collect = function(obj, iteratee, context) {
    iteratee = cb(iteratee, context);
    var keys = !isArrayLike(obj) && _.keys(obj),
        length = (keys || obj).length,
        results = Array(length);
    for (var index = 0; index < length; index++) {
      var currentKey = keys ? keys[index] : index;
      results[index] = iteratee(obj[currentKey], currentKey, obj);
    }
    return results;
  };

  // 左または右の反復還元作用を作成します。
  function createReduce(dir) {
    // arguments.lengthを使用するなど、最適化されたイテレータ関数
    // deoptimizeます主な機能には、＃1991を参照してください。
    function iterator(obj, iteratee, memo, keys, index, length) {
      for (; index >= 0 && index < length; index += dir) {
        var currentKey = keys ? keys[index] : index;
        memo = iteratee(memo, obj[currentKey], currentKey, obj);
      }
      return memo;
    }

    return function(obj, iteratee, memo, context) {
      iteratee = optimizeCb(iteratee, context, 4);
      var keys = !isArrayLike(obj) && _.keys(obj),
          length = (keys || obj).length,
          index = dir > 0 ? 0 : length - 1;
      // 何も提供されていない場合は初期値を決定します。
      if (arguments.length < 3) {
        memo = obj[keys ? keys[index] : index];
        index += dir;
      }
      return iterator(obj, iteratee, memo, keys, index, length);
    };
  }

  // **値のリストから単一の結果を構築**減らし、別名 `inject`、
  // または `foldl`。
  _.reduce = _.foldl = _.inject = createReduce(1);

  // またfoldr` `として知られている削減の右結合バージョン
  _.reduceRight = _.foldr = createReduce(-1);

  // 真実の試験に合格する最初の値を返します。 detect` `というエイリアス。
  _.find = _.detect = function(obj, predicate, context) {
    var key;
    if (isArrayLike(obj)) {
      key = _.findIndex(obj, predicate, context);
    } else {
      key = _.findKey(obj, predicate, context);
    }
    if (key !== void 0 && key !== -1) return obj[key];
  };

  // 真実の試験に合格するすべての要素を返します。
  // SELECT` `というエイリアス。
  _.filter = _.select = function(obj, predicate, context) {
    var results = [];
    predicate = cb(predicate, context);
    _.each(obj, function(value, index, list) {
      if (predicate(value, index, list)) results.push(value);
    });
    return results;
  };

  // 真実のテストが失敗しているすべての要素を返します。
  _.reject = function(obj, predicate, context) {
    return _.filter(obj, _.negate(cb(predicate)), context);
  };

  // すべての要素が、真実の試験と一致するかどうかを確認します。
  // all` `としてエイリアス。
  _.every = _.all = function(obj, predicate, context) {
    predicate = cb(predicate, context);
    var keys = !isArrayLike(obj) && _.keys(obj),
        length = (keys || obj).length;
    for (var index = 0; index < length; index++) {
      var currentKey = keys ? keys[index] : index;
      if (!predicate(obj[currentKey], currentKey, obj)) return false;
    }
    return true;
  };

  // オブジェクト内の少なくとも1つの要素が真実の試験と一致するかどうかを確認します。
  // any` `というエイリアス。
  _.some = _.any = function(obj, predicate, context) {
    predicate = cb(predicate, context);
    var keys = !isArrayLike(obj) && _.keys(obj),
        length = (keys || obj).length;
    for (var index = 0; index < length; index++) {
      var currentKey = keys ? keys[index] : index;
      if (predicate(obj[currentKey], currentKey, obj)) return true;
    }
    return false;
  };

  // 配列やオブジェクトが、指定された項目（ `===`使用して）が含まれているかどうかを確認します。
  // エイリアスincludes`と `include``として。
  _.contains = _.includes = _.include = function(obj, item, fromIndex, guard) {
    if (!isArrayLike(obj)) obj = _.values(obj);
    if (typeof fromIndex != 'number' || guard) fromIndex = 0;
    return _.indexOf(obj, item, fromIndex) >= 0;
  };

  // コレクション内のすべての項目に（引数を持つ）メソッドを呼び出します。
  _.invoke = function(obj, method) {
    var args = slice.call(arguments, 2);
    var isFunc = _.isFunction(method);
    return _.map(obj, function(value) {
      var func = isFunc ? method : value[method];
      return func == null ? func : func.apply(value, args);
    });
  };

  // プロパティを取得： `map`の一般的な使用例の簡易バージョン。
  _.pluck = function(obj, key) {
    return _.map(obj, _.property(key));
  };

  // filter` `の一般的な使用例コンビニエンス版：オブジェクトのみを選択します。
  // [値]のペア：特定の `のキーを含みます。
  _.where = function(obj, attrs) {
    return _.filter(obj, _.matcher(attrs));
  };

  // find` `の一般的な使用例コンビニエンス版：最初のオブジェクトを取得します
  // [値]のペア：特定の `のキーを含みます。
  _.findWhere = function(obj, attrs) {
    return _.find(obj, _.matcher(attrs));
  };

  // 最大の要素（または要素に基づく計算）を返します。
  _.max = function(obj, iteratee, context) {
    var result = -Infinity, lastComputed = -Infinity,
        value, computed;
    if (iteratee == null && obj != null) {
      obj = isArrayLike(obj) ? obj : _.values(obj);
      for (var i = 0, length = obj.length; i < length; i++) {
        value = obj[i];
        if (value > result) {
          result = value;
        }
      }
    } else {
      iteratee = cb(iteratee, context);
      _.each(obj, function(value, index, list) {
        computed = iteratee(value, index, list);
        if (computed > lastComputed || computed === -Infinity && result === -Infinity) {
          result = value;
          lastComputed = computed;
        }
      });
    }
    return result;
  };

  // 最小要素（または要素に基づく計算）を返します。
  _.min = function(obj, iteratee, context) {
    var result = Infinity, lastComputed = Infinity,
        value, computed;
    if (iteratee == null && obj != null) {
      obj = isArrayLike(obj) ? obj : _.values(obj);
      for (var i = 0, length = obj.length; i < length; i++) {
        value = obj[i];
        if (value < result) {
          result = value;
        }
      }
    } else {
      iteratee = cb(iteratee, context);
      _.each(obj, function(value, index, list) {
        computed = iteratee(value, index, list);
        if (computed < lastComputed || computed === Infinity && result === Infinity) {
          result = value;
          lastComputed = computed;
        }
      });
    }
    return result;
  };

  // の現代版を使用して、コレクションをシャッフル
  // 【フィッシャーイェーツシャッフル]（http://en.wikipedia.org/wiki/Fisher-Yates_shuffle）。
  _.shuffle = function(obj) {
    var set = isArrayLike(obj) ? obj : _.values(obj);
    var length = set.length;
    var shuffled = Array(length);
    for (var index = 0, rand; index < length; index++) {
      rand = _.random(0, index);
      if (rand !== index) shuffled[index] = shuffled[rand];
      shuffled[rand] = set[index];
    }
    return shuffled;
  };

  // サンプル** nは**コレクションからランダムな値。
  // ** nは**指定されていない場合は、単一のランダム要素を返します。
  // 内部 `guard`引数は、それがmap``で作業することができます。
  _.sample = function(obj, n, guard) {
    if (n == null || guard) {
      if (!isArrayLike(obj)) obj = _.values(obj);
      return obj[_.random(obj.length - 1)];
    }
    return _.shuffle(obj).slice(0, Math.max(0, n));
  };

  // iterateeによって生成される基準によって、オブジェクトの値を並べ替えます。
  _.sortBy = function(obj, iteratee, context) {
    iteratee = cb(iteratee, context);
    return _.pluck(_.map(obj, function(value, index, list) {
      return {
        value: value,
        index: index,
        criteria: iteratee(value, index, list)
      };
    }).sort(function(left, right) {
      var a = left.criteria;
      var b = right.criteria;
      if (a !== b) {
        if (a > b || a === void 0) return 1;
        if (a < b || b === void 0) return -1;
      }
      return left.index - right.index;
    }), 'value');
  };

  // 操作 "でグループ"集約のために使用される内部機能。
  var group = function(behavior) {
    return function(obj, iteratee, context) {
      var result = {};
      iteratee = cb(iteratee, context);
      _.each(obj, function(value, index) {
        var key = iteratee(value, index, obj);
        behavior(result, value, key);
      });
      return result;
    };
  };

  // 基準によってグループオブジェクトの値を。文字列の属性のいずれかを渡します
  // グループによって、または基準を返す関数への。
  _.groupBy = group(function(result, value, key) {
    if (_.has(result, key)) result[key].push(value); else result[key] = [value];
  });

  // groupBy` `と似ていますが、の基準によって、インデックス、オブジェクトの値を、
  // あなたのインデックス値は一意であることがわかっているとき。
  _.indexBy = group(function(result, value, key) {
    result[key] = value;
  });

  // 一定の基準によって物体そのグループのインスタンスをカウントします。パス
  // カウントする文字列属性、または返す関数のいずれか
  // 基準
  _.countBy = group(function(result, value, key) {
    if (_.has(result, key)) result[key]++; else result[key] = 1;
  });

  // 安全のiterable何から実際のライブの配列を作成します。
  _.toArray = function(obj) {
    if (!obj) return [];
    if (_.isArray(obj)) return slice.call(obj);
    if (isArrayLike(obj)) return _.map(obj, _.identity);
    return _.values(obj);
  };

  // オブジェクト内の要素の数を返します。
  _.size = function(obj) {
    if (obj == null) return 0;
    return isArrayLike(obj) ? obj.length : _.keys(obj).length;
  };

  // その要素がすべて満たす、指定された1：2の配列にコレクションを分割します
  // 述語、およびその要素がすべて述語を満たさない1。
  _.partition = function(obj, predicate, context) {
    predicate = cb(predicate, context);
    var pass = [], fail = [];
    _.each(obj, function(value, key, obj) {
      (predicate(value, key, obj) ? pass : fail).push(value);
    });
    return [pass, fail];
  };

  // 配列関数
  // ---------------

  // 配列の最初の要素を取得します。渡すと、** nは**最初のNを返します。
  // 配列の値。 head`と `take``というエイリアス。 **ガード**チェック
  // それは_.map` `で作業することができます。
  _.first = _.head = _.take = function(array, n, guard) {
    if (array == null) return void 0;
    if (n == null || guard) return array[0];
    return _.initial(array, array.length - n);
  };

  // 配列の最後のエントリ以外のすべてを返します。上の特に有用な
  // argumentsオブジェクト。 **渡すのn **のすべての値を返します。
  // 最後N.除く配列
  _.initial = function(array, n, guard) {
    return slice.call(array, 0, Math.max(0, array.length - (n == null || guard ? 1 : n)));
  };

  // 配列の最後の要素を取得します。 ** nは**最後のN戻ります渡します
  // 配列の値。
  _.last = function(array, n, guard) {
    if (array == null) return void 0;
    if (n == null || guard) return array[array.length - 1];
    return _.rest(array, Math.max(0, array.length - n));
  };

  // 配列の最初のエントリ以外のすべてを返します。 tail`と `drop``というエイリアス。
  // argumentsオブジェクトに特に便利。 ** nは**を渡すと、返されます
  // 配列内の残りのN値。
  _.rest = _.tail = _.drop = function(array, n, guard) {
    return slice.call(array, n == null || guard ? 1 : n);
  };

  // 配列からすべてfalsy値をトリミングします。
  _.compact = function(array) {
    return _.filter(array, _.identity);
  };

  // 再帰 `flatten`関数の内部実装。
  var flatten = function(input, shallow, strict, startIndex) {
    var output = [], idx = 0;
    for (var i = startIndex || 0, length = getLength(input); i < length; i++) {
      var value = input[i];
      if (isArrayLike(value) && (_.isArray(value) || _.isArguments(value))) {
        //flatten current level of array or arguments object
        if (!shallow) value = flatten(value, shallow, strict);
        var j = 0, len = value.length;
        output.length += len;
        while (j < len) {
          output[idx++] = value[j++];
        }
      } else if (!strict) {
        output[idx++] = value;
      }
    }
    return output;
  };

  // 配列、のいずれかを再帰的に（デフォルト）、または単に1レベルを平ら。
  _.flatten = function(array, shallow) {
    return flatten(array, shallow, false);
  };

  // 指定された値（複数可）を含有していない配列のバージョンを返します。
  _.without = function(array) {
    return _.difference(array, slice.call(arguments, 1));
  };

  // 配列の重複のないバージョンを生成します。配列は、既に持っている場合
  // ソートされて、あなたはより高速なアルゴリズムを使用するオプションがあります。
  // unique` `としてエイリアス。
  _.uniq = _.unique = function(array, isSorted, iteratee, context) {
    if (!_.isBoolean(isSorted)) {
      context = iteratee;
      iteratee = isSorted;
      isSorted = false;
    }
    if (iteratee != null) iteratee = cb(iteratee, context);
    var result = [];
    var seen = [];
    for (var i = 0, length = getLength(array); i < length; i++) {
      var value = array[i],
          computed = iteratee ? iteratee(value, i, array) : value;
      if (isSorted) {
        if (!i || seen !== computed) result.push(value);
        seen = computed;
      } else if (iteratee) {
        if (!_.contains(seen, computed)) {
          seen.push(computed);
          result.push(value);
        }
      } else if (!_.contains(result, value)) {
        result.push(value);
      }
    }
    return result;
  };

  // のすべてから、各個別要素：組合を含む配列を生成
  // 渡された配列。
  _.union = function() {
    return _.uniq(flatten(arguments, true, true));
  };

  // すべての間で共有されるすべてのアイテムを含む配列を生成
  // 渡された配列
  _.intersection = function(array) {
    var result = [];
    var argsLength = arguments.length;
    for (var i = 0, length = getLength(array); i < length; i++) {
      var item = array[i];
      if (_.contains(result, item)) continue;
      for (var j = 1; j < argsLength; j++) {
        if (!_.contains(arguments[j], item)) break;
      }
      if (j === argsLength) result.push(item);
    }
    return result;
  };

  // 1配列および他の配列の数との差を取ります。
  // ちょうど最初の配列中に存在する元素のみが残ります
  _.difference = function(array) {
    var rest = flatten(arguments, true, true, 1);
    return _.filter(array, function(value){
      return !_.contains(rest, value);
    });
  };

  // 単一の配列に複数のリストを一緒にジップ - 要素を共有
  // インデックスは、一緒に行きます。
  _.zip = function() {
    return _.unzip(arguments);
  };

  // _.zipの補数。解凍し、配列およびグループの配列を受け入れ
  // 共有指数の各配列の要素
  _.unzip = function(array) {
    var length = array && _.max(array, getLength).length || 0;
    var result = Array(length);

    for (var index = 0; index < length; index++) {
      result[index] = _.pluck(array, index);
    }
    return result;
  };

  // オブジェクトにリストを変換します。 `` [キー、値]の単一の配列を渡します
  // ペア、または同じ長さの2つの平行配列 - キーのいずれか、との1
  // 対応する値。
  _.object = function(list, values) {
    var result = {};
    for (var i = 0, length = getLength(list); i < length; i++) {
      if (values) {
        result[list[i]] = values[i];
      } else {
        result[list[i][0]] = list[i][1];
      }
    }
    return result;
  };

  // findIndexとfindLastIndex関数を作成するジェネレータ関数
  function createPredicateIndexFinder(dir) {
    return function(array, predicate, context) {
      predicate = cb(predicate, context);
      var length = getLength(array);
      var index = dir > 0 ? 0 : length - 1;
      for (; index >= 0 && index < length; index += dir) {
        if (predicate(array[index], index, array)) return index;
      }
      return -1;
    };
  }

  // 配列のような述語テストに合格した上で最初のインデックスを返します。
  _.findIndex = createPredicateIndexFinder(1);
  _.findLastIndex = createPredicateIndexFinder(-1);

  // これで最小のインデックスを把握するためにコンパレータ機能を使用します
  // 秩序を維持するようにオブジェクトを挿入する必要があります。バイナリ検索を使用します。
  _.sortedIndex = function(array, obj, iteratee, context) {
    iteratee = cb(iteratee, context, 1);
    var value = iteratee(obj);
    var low = 0, high = getLength(array);
    while (low < high) {
      var mid = Math.floor((low + high) / 2);
      if (iteratee(array[mid]) < value) low = mid + 1; else high = mid;
    }
    return low;
  };

  // indexOfおよびlastIndexOfでも関数を作成するジェネレータ関数
  function createIndexFinder(dir, predicateFind, sortedIndex) {
    return function(array, item, idx) {
      var i = 0, length = getLength(array);
      if (typeof idx == 'number') {
        if (dir > 0) {
            i = idx >= 0 ? idx : Math.max(idx + length, i);
        } else {
            length = idx >= 0 ? Math.min(idx + 1, length) : idx + length + 1;
        }
      } else if (sortedIndex && idx && length) {
        idx = sortedIndex(array, item);
        return array[idx] === item ? idx : -1;
      }
      if (item !== item) {
        idx = predicateFind(slice.call(array, i, length), _.isNaN);
        return idx >= 0 ? idx + i : -1;
      }
      for (idx = dir > 0 ? i : length - 1; idx >= 0 && idx < length; idx += dir) {
        if (array[idx] === item) return idx;
      }
      return -1;
    };
  }

  // 配列内のアイテムの最初に出現する位置を返し、
  // または-1の項目が配列に含まれていない場合。
  // 配列が大きく、すでにソート順になっている場合は、 `true`をを渡します
  // **関数issorted **バイナリ検索を使用するため。
  _.indexOf = createIndexFinder(1, _.findIndex, _.sortedIndex);
  _.lastIndexOf = createIndexFinder(-1, _.findLastIndex);

  // 等差数列を含む整数型配列を生成します。のポート
  // ネイティブのPython `範囲（）`関数。見る
  // [Pythonドキュメント]（http://docs.python.org/library/functions.html#range）。
  _.range = function(start, stop, step) {
    if (stop == null) {
      stop = start || 0;
      start = 0;
    }
    step = step || 1;

    var length = Math.max(Math.ceil((stop - start) / step), 0);
    var range = Array(length);

    for (var idx = 0; idx < length; idx++, start += step) {
      range[idx] = start;
    }

    return range;
  };

  // ファンクション（エヘン）機能
  // ------------------

  // コンストラクタとしての機能を実行するか否かを判断します
  // 指定された引数を使用し、または正常な機能
  var executeBound = function(sourceFunc, boundFunc, context, callingContext, args) {
    if (!(callingContext instanceof boundFunc)) return sourceFunc.apply(context, args);
    var self = baseCreate(sourceFunc.prototype);
    var result = sourceFunc.apply(self, args);
    if (_.isObject(result)) return result;
    return self;
  };

  // 指定されたオブジェクト（ `this`を割り当て、および引数にバインドされた関数を作成し、
  // 必要に応じて）。デリゲートに**のECMAScript 5 **のネイティブ `Function.bind`場合
  // 利用可能。
  _.bind = function(func, context) {
    if (nativeBind && func.bind === nativeBind) return nativeBind.apply(func, slice.call(arguments, 1));
    if (!_.isFunction(func)) throw new TypeError('Bind must be called on a function');
    var args = slice.call(arguments, 2);
    var bound = function() {
      return executeBound(func, bound, context, this, args.concat(slice.call(arguments)));
    };
    return bound;
  };

  // 部分的にそののいくつかを持っていたバージョンを作成することにより、関数を適用
  // 引数は、そのダイナミック `this`コンテキストを変更することなく、予め充填されました。 _行為
  // プレースホルダとして、引数の任意の組み合わせが予め充填されることを可能にします。
  _.partial = function(func) {
    var boundArgs = slice.call(arguments, 1);
    var bound = function() {
      var position = 0, length = boundArgs.length;
      var args = Array(length);
      for (var i = 0; i < length; i++) {
        args[i] = boundArgs[i] === _ ? arguments[position++] : boundArgs[i];
      }
      while (position < arguments.length) args.push(arguments[position++]);
      return executeBound(func, bound, this, this, args);
    };
    return bound;
  };

  // そのオブジェクトへのオブジェクトのメソッドの数をバインドします。残りの引数
  // バインドするメソッド名です。すべてのコールバックすることを確保するために有用
  // オブジェクトで定義され、それに属しています。
  _.bindAll = function(obj) {
    var i, length = arguments.length, key;
    if (length <= 1) throw new Error('bindAll must be passed function names');
    for (i = 1; i < length; i++) {
      key = arguments[i];
      obj[key] = _.bind(obj[key], obj);
    }
    return obj;
  };

  // その結果を格納することによって、高価な関数をメモ化します。
  _.memoize = function(func, hasher) {
    var memoize = function(key) {
      var cache = memoize.cache;
      var address = '' + (hasher ? hasher.apply(this, arguments) : key);
      if (!_.has(cache, address)) cache[address] = func.apply(this, arguments);
      return cache[address];
    };
    memoize.cache = {};
    return memoize;
  };

  // ミリ秒単位の与えられた数のための機能を遅延させ、その後、呼び出し
  // 指定された引数を持つこと。
  _.delay = function(func, wait) {
    var args = slice.call(arguments, 2);
    return setTimeout(function(){
      return func.apply(null, args);
    }, wait);
  };

  // 現在のコールスタックを持っていた後に実行するようにスケジュール設定、機能を延期
  // クリア。
  _.defer = _.partial(_.delay, _, 1);

  // 呼び出されたときに、唯一の最も一度トリガされることは、関数を返します。
  // 時間の指定されたウィンドウ中。通常は、スロットル機能が実行されます
  // これまで以上に `wait`期間ごとに何度も行くことなく、それができる限り
  // あなたはリーディングエッジ上での実行を無効にしたい場合は、合格
  // `{リーディング：偽}`。後縁、同上上での実行を無効にします。
  _.throttle = function(func, wait, options) {
    var context, args, result;
    var timeout = null;
    var previous = 0;
    if (!options) options = {};
    var later = function() {
      previous = options.leading === false ? 0 : _.now();
      timeout = null;
      result = func.apply(context, args);
      if (!timeout) context = args = null;
    };
    return function() {
      var now = _.now();
      if (!previous && options.leading === false) previous = now;
      var remaining = wait - (now - previous);
      context = this;
      args = arguments;
      if (remaining <= 0 || remaining > wait) {
        if (timeout) {
          clearTimeout(timeout);
          timeout = null;
        }
        previous = now;
        result = func.apply(context, args);
        if (!timeout) context = args = null;
      } else if (!timeout && options.trailing !== false) {
        timeout = setTimeout(later, remaining);
      }
      return result;
    };
  };

  // 限り、それが呼び出され続けるようにすることを、機能しないであろう返します
  // トリガすること。それが求められて停止した後に関数が呼び出されます
  // Nミリ秒。 `immediate`が渡された場合、上の機能を誘発します
  // 代わりに、末尾の前縁、。
  _.debounce = function(func, wait, immediate) {
    var timeout, args, context, timestamp, result;

    var later = function() {
      var last = _.now() - timestamp;

      if (last < wait && last >= 0) {
        timeout = setTimeout(later, wait - last);
      } else {
        timeout = null;
        if (!immediate) {
          result = func.apply(context, args);
          if (!timeout) context = args = null;
        }
      }
    };

    return function() {
      context = this;
      args = arguments;
      timestamp = _.now();
      var callNow = immediate && !timeout;
      if (!timeout) timeout = setTimeout(later, wait);
      if (callNow) {
        result = func.apply(context, args);
        context = args = null;
      }

      return result;
    };
  };

  // 第二に引数として渡された最初の関数を返します、
  // 前後のコードを実行し、あなたは引数を調整することを可能にし、
  // 条件付きで元の関数を実行します。
  _.wrap = function(func, wrapper) {
    return _.partial(wrapper, func);
  };

  // 渡された述語の否定バージョンを返します。
  _.negate = function(predicate) {
    return function() {
      return !predicate.apply(this, arguments);
    };
  };

  // 各機能のリストの組成物である関数を返します。
  // 次の関数の戻り値を消費します。
  _.compose = function() {
    var args = arguments;
    var start = args.length - 1;
    return function() {
      var i = start;
      var result = args[start].apply(this, arguments);
      while (i--) result = args[i].call(this, result);
      return result;
    };
  };

  // のみにし、N番目の呼び出しの後に実行される関数を返します。
  _.after = function(times, func) {
    return function() {
      if (--times < 1) {
        return func.apply(this, arguments);
      }
    };
  };

  // 唯一のN番目のコールを（は含まない）まで実行される関数を返します。
  _.before = function(times, func) {
    var memo;
    return function() {
      if (--times > 0) {
        memo = func.apply(this, arguments);
      }
      if (times <= 1) func = null;
      return memo;
    };
  };

  // どんなに多くても1つの時間に実行される機能を、返します
  // 多くの場合、あなたはそれを呼び出します。遅延初期化のための有用な。
  _.once = _.partial(_.before, 2);

  // オブジェクト関数
  // ----------------

  // で... `キーの`によって反復ので、見逃すことはありませんIE <9内のキー。
  var hasEnumBug = !{toString: null}.propertyIsEnumerable('toString');
  var nonEnumerableProps = ['valueOf', 'isPrototypeOf', 'toString',
                      'propertyIsEnumerable', 'hasOwnProperty', 'toLocaleString'];

  function collectNonEnumProps(obj, keys) {
    var nonEnumIdx = nonEnumerableProps.length;
    var constructor = obj.constructor;
    var proto = (_.isFunction(constructor) && constructor.prototype) || ObjProto;

    // コンストラクタは特殊なケースです。
    var prop = 'constructor';
    if (_.has(obj, prop) && !_.contains(keys, prop)) keys.push(prop);

    while (nonEnumIdx--) {
      prop = nonEnumerableProps[nonEnumIdx];
      if (prop in obj && obj[prop] !== proto[prop] && !_.contains(keys, prop)) {
        keys.push(prop);
      }
    }
  }

  // オブジェクト自身のプロパティの名前を取得します。
  // デリゲートに**のECMAScript 5 **のネイティブ `Object.keys`
  _.keys = function(obj) {
    if (!_.isObject(obj)) return [];
    if (nativeKeys) return nativeKeys(obj);
    var keys = [];
    for (var key in obj) if (_.has(obj, key)) keys.push(key);
    // Ahem, IE < 9.
    if (hasEnumBug) collectNonEnumProps(obj, keys);
    return keys;
  };

  // オブジェクトのすべてのプロパティ名を取得します。
  _.allKeys = function(obj) {
    if (!_.isObject(obj)) return [];
    var keys = [];
    for (var key in obj) keys.push(key);
    // Ahem, IE < 9.
    if (hasEnumBug) collectNonEnumProps(obj, keys);
    return keys;
  };

  // オブジェクトのプロパティの値を取得します。
  _.values = function(obj) {
    var keys = _.keys(obj);
    var length = keys.length;
    var values = Array(length);
    for (var i = 0; i < length; i++) {
      values[i] = obj[keys[i]];
    }
    return values;
  };

  // オブジェクトの各要素にiterateeを適用した結果を返します
  // _.mapとは対照的に、それは、オブジェクトを返します
  _.mapObject = function(obj, iteratee, context) {
    iteratee = cb(iteratee, context);
    var keys =  _.keys(obj),
          length = keys.length,
          results = {},
          currentKey;
      for (var index = 0; index < length; index++) {
        currentKey = keys[index];
        results[currentKey] = iteratee(obj[currentKey], currentKey, obj);
      }
      return results;
  };

  // `[キー、値]`ペアのリストにオブジェクトを変換します。
  _.pairs = function(obj) {
    var keys = _.keys(obj);
    var length = keys.length;
    var pairs = Array(length);
    for (var i = 0; i < length; i++) {
      pairs[i] = [keys[i], obj[keys[i]]];
    }
    return pairs;
  };

  // オブジェクトのキーと値を反転。値はシリアライズ可能である必要があります。
  _.invert = function(obj) {
    var result = {};
    var keys = _.keys(obj);
    for (var i = 0, length = keys.length; i < length; i++) {
      result[obj[keys[i]]] = keys[i];
    }
    return result;
  };

  // オブジェクトで利用可能な関数名のソートされたリストを返します。
  // methods` `としてエイリアス
  _.functions = _.methods = function(obj) {
    var names = [];
    for (var key in obj) {
      if (_.isFunction(obj[key])) names.push(key);
    }
    return names.sort();
  };

  // 渡されたオブジェクト（複数可）内のすべてのプロパティで指定されたオブジェクトを拡張します。
  _.extend = createAssigner(_.allKeys);

  // 渡されたオブジェクト（複数可）内のすべての独自のプロパティで指定されたオブジェクトを割り当て
  // (https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object/assign)
  _.extendOwn = _.assign = createAssigner(_.keys);

  // 
述語テストに合格したオブジェクトの最初のキーを返します
  _.findKey = function(obj, predicate, context) {
    predicate = cb(predicate, context);
    var keys = _.keys(obj), key;
    for (var i = 0, length = keys.length; i < length; i++) {
      key = keys[i];
      if (predicate(obj[key], key, obj)) return key;
    }
  };

  // 唯一のホワイトリストに登録されたプロパティを含むオブジェクトのコピーを返します。
  _.pick = function(object, oiteratee, context) {
    var result = {}, obj = object, iteratee, keys;
    if (obj == null) return result;
    if (_.isFunction(oiteratee)) {
      keys = _.allKeys(obj);
      iteratee = optimizeCb(oiteratee, context);
    } else {
      keys = flatten(arguments, false, false, 1);
      iteratee = function(value, key, obj) { return key in obj; };
      obj = Object(obj);
    }
    for (var i = 0, length = keys.length; i < length; i++) {
      var key = keys[i];
      var value = obj[key];
      if (iteratee(value, key, obj)) result[key] = value;
    }
    return result;
  };

   // ブラックリストに載ってプロパティずにオブジェクトのコピーを返します。
  _.omit = function(obj, iteratee, context) {
    if (_.isFunction(iteratee)) {
      iteratee = _.negate(iteratee);
    } else {
      var keys = _.map(flatten(arguments, false, false, 1), String);
      iteratee = function(value, key) {
        return !_.contains(keys, key);
      };
    }
    return _.pick(obj, iteratee, context);
  };

  // デフォルトプロパティを持つ指定されたオブジェクトに入力します。
  _.defaults = createAssigner(_.allKeys, true);

  // 指定されたプロトタイプオブジェクトを継承したオブジェクトを作成します。
  // 追加のプロパティが提供されているfはそれらが追加され
  // 作成されたオブジェクト。
  _.create = function(prototype, props) {
    var result = baseCreate(prototype);
    if (props) _.extendOwn(result, props);
    return result;
  };

  // オブジェクトの複製（浅いクローン化）を作成します。
  _.clone = function(obj) {
    if (!_.isObject(obj)) return obj;
    return _.isArray(obj) ? obj.slice() : _.extend({}, obj);
  };

  // OBJでインターセプタを呼び出し、その後、OBJを返します。
  // この方法の主な目的は、に、メソッドチェーン "を活用」することです
  // チェーン内の中間結果に対して操作を実行するため。
  _.tap = function(obj, interceptor) {
    interceptor(obj);
    return obj;
  };

  // [値]のペア：オブジェクトは `キーの所定のセットを持っているかどうかを返します。
  _.isMatch = function(object, attrs) {
    var keys = _.keys(attrs), length = keys.length;
    if (object == null) return !length;
    var obj = Object(object);
    for (var i = 0; i < length; i++) {
      var key = keys[i];
      if (attrs[key] !== obj[key] || !(key in obj)) return false;
    }
    return true;
  };


  // `isEqual`用の内部再帰的な比較関数。
  var eq = function(a, b, aStack, bStack) {
    // 同一のオブジェクトが等しいです。 `0 === -0`が、それらは同一ではありません。
    // [ハーモニー `egal`提案]（http://wiki.ecmascript.org/doku.php?id=harmony:egal）を参照してください。
    if (a === b) return a !== 0 || 1 / a === 1 / b;
    // `nullは== undefined`ので厳密な比較が必要です。
    if (a == null || b == null) return a === b;
    // 任意のラップされたオブジェクトをアンラップ。
    if (a instanceof _) a = a._wrapped;
    if (b instanceof _) b = b._wrapped;
    // `[[クラス]]`名前を比較します。
    var className = toString.call(a);
    if (className !== toString.call(b)) return false;
    switch (className) {
      // 文字列、数値、正規表現、日付、およびブール値は値で比較されます。
      case '[object RegExp]':
      // 正規表現は、比較のために文字列に強制変換されます（注： '' + / A / I === '/ A / I'）
      case '[object String]':
        // プリミティブとそれに対応するオブジェクトラッパーは等価です。このように、 `" 5 "`です
        // `新しいString（" 5 "）`に相当します。
        return '' + a === '' + b;
      case '[object Number]':
        // `NaN`sは同等のが、非反射的です。
        // オブジェクト（NaNは）はNaNと同等です
        if (+a !== +a) return +b !== +b;
        // `egal`比較は、他の数値を行います。
        return +a === 0 ? 1 / +a === 1 / b : +a === +b;
      case '[object Date]':
      case '[object Boolean]':
        // 数値プリミティブ値に日付やブール値を強要。日付はで比較され、その
        // ミリ秒表現。ミリ秒の表現とその無効な日付に注意してください。
        // `NaN`の等価ではありません。
        return +a === +b;
    }

    var areArrays = className === '[object Array]';
    if (!areArrays) {
      if (typeof a != 'object' || typeof b != 'object') return false;

      // 別のコンストラクタを持つオブジェクトは等価ではありませんが、 `Object`sまたは` Array`s
      // 異なるフレームからのものです。
      var aCtor = a.constructor, bCtor = b.constructor;
      if (aCtor !== bCtor && !(_.isFunction(aCtor) && aCtor instanceof aCtor &&
                               _.isFunction(bCtor) && bCtor instanceof bCtor)
                          && ('constructor' in a && 'constructor' in b)) {
        return false;
      }
    }
    // 環状構造のための平等を前提としています。サイクリックを検出するためのアルゴリズム
    // structures is adapted from ES 5.1 section 15.12.3, abstract operation `JO`.

    // 横断オブジェクトのスタックを初期化します。
    // 我々は唯一のオブジェクトと配列の比較のためにそれらを必要とするので、それはここで完了です。
    aStack = aStack || [];
    bStack = bStack || [];
    var length = aStack.length;
    while (length--) {
      // リニア検索。パフォーマンスの数に反比例します
      // ユニークなネストされた構造。
      if (aStack[length] === a) return bStack[length] === b;
    }

    // 横断オブジェクトのスタックに最初のオブジェクトを追加します。
    aStack.push(a);
    bStack.push(b);

    // 再帰的オブジェクトと配列を比較します。
    if (areArrays) {
      // 深い比較が必要であるかどうかを決定するために、配列の長さを比較します。
      length = a.length;
      if (length !== b.length) return false;
      // ディープは、数値以外の特性を無視して、内容を比較します。
      while (length--) {
        if (!eq(a[length], b[length], aStack, bStack)) return false;
      }
    } else {
      // ディープは、オブジェクトを比較します。
      var keys = _.keys(a), key;
      length = keys.length;
      // E両方のオブジェクトが深い平等を比較する前に、プロパティの同じ番号が含まれnsure
      if (_.keys(b).length !== length) return false;
      while (length--) {
        // Deep compare each member
        key = keys[length];
        if (!(_.has(b, key) && eq(a[key], b[key], aStack, bStack))) return false;
      }
    }
    // 両方のオブジェクトが深い平等を比較する前に、プロパティの同じ番号が含まれていることを確認してください。.
    aStack.pop();
    bStack.pop();
    return true;
  };

  // 2つのオブジェクトが等しいかどうかを確認するために、深い比較を行います。
  _.isEqual = function(a, b) {
    return eq(a, b);
  };

  // 2つのオブジェクトが等しいかどうかを確認するために、深い比較を行います。
  // 「空」のオブジェクトが列挙自社の特性を持っていません。
  _.isEmpty = function(obj) {
    if (obj == null) return true;
    if (isArrayLike(obj) && (_.isArray(obj) || _.isString(obj) || _.isArguments(obj))) return obj.length === 0;
    return _.keys(obj).length === 0;
  };

  // 指定された値は、DOM要素はありますか？
  _.isElement = function(obj) {
    return !!(obj && obj.nodeType === 1);
  };

  // 指定された値は、DOM要素はありますか？
  // ECMA5のネイティブArray.isArrayに委譲
  _.isArray = nativeIsArray || function(obj) {
    return toString.call(obj) === '[object Array]';
  };

  // 与えられた変数は、オブジェクトですか？
  _.isObject = function(obj) {
    var type = typeof obj;
    return type === 'function' || type === 'object' && !!obj;
  };

  // isArguments、isFunction、isString、ISNUMBER、ISDATE、isRegExp、ののisError：一部のisTypeメソッドを追加します。
  _.each(['Arguments', 'Function', 'String', 'Number', 'Date', 'RegExp', 'Error'], function(name) {
    _['is' + name] = function(obj) {
      return toString.call(obj) === '[object ' + name + ']';
    };
  });


  // ブラウザでメソッドの代替バージョンを定義します（エヘン、IE <9）、ここで、
  // 任意の検査可能「引数」タイプがありません。
  if (!_.isArguments(arguments)) {
    _.isArguments = function(obj) {
      return _.has(obj, 'callee');
    };
  }

  // 適切であれば、 `isFunction`を最適化します。古いV8にいくつかのtypeof演算のバグを回避、
  // IE 11（＃1621）、およびSafari 8（＃1929）。
  if (typeof /./ != 'function' && typeof Int8Array != 'object') {
    _.isFunction = function(obj) {
      return typeof obj == 'function' || false;
    };
  }

  // 指定されたオブジェクトは、有限個のですか？
  _.isFinite = function(obj) {
    return isFinite(obj) && !isNaN(parseFloat(obj));
  };

  // 指定された値は `NaN`ですか？ （NaNは自分自身に等しくない数のみです）。
  _.isNaN = function(obj) {
    return _.isNumber(obj) && obj !== +obj;
  };

  // 指定された値は論理値ですか？
  _.isBoolean = function(obj) {
    return obj === true || obj === false || toString.call(obj) === '[object Boolean]';
  };

  // 指定された値は、指定された値がnullに等しい？ブールですか？
  _.isNull = function(obj) {
    return obj === null;
  };

  // 与えられた変数が定義されていませんか？
  _.isUndefined = function(obj) {
    return obj === void 0;
  };

  // オブジェクトが、指定されたプロパティがdirectlyundefinedているかどうかをチェックするために与えられた変数のショートカット機能はありますか？
  // 自体に（つまり、いないプロトタイプ上）。
  _.has = function(obj, key) {
    return obj != null && hasOwnProperty.call(obj, key);
  };

  // ユーティリティ関数
  // -----------------

  // * noConflict *モードで実行しUnderscore.js、そのに `_`変数を返します
  // 以前の所有者。下線オブジェクトへの参照を返します。
  _.noConflict = function() {
    root._ = previousUnderscore;
    return this;
  };

  // デフォルトiterateesの周り恒等関数にしてください。
  _.identity = function(value) {
    return value;
  };

  // 述語生成機能。下線の外に便利。
  _.constant = function(value) {
    return function() {
      return value;
    };
  };

  _.noop = function(){};

  _.property = property;

  // 指定されたプロパティを返す指定されたオブジェクトのための関数を生成します。
  _.propertyOf = function(obj) {
    return obj == null ? function(){} : function(key) {
      return obj[key];
    };
  };

  // オブジェクトは、所定のセットを持っているかどうかをチェックするための述語を返します。
  // `キー：[値]のペア。
  _.matcher = _.matches = function(attrs) {
    attrs = _.extendOwn({}, attrs);
    return function(obj) {
      return _.isMatch(obj, attrs);
    };
  };

  // 機能**のn **回を実行します。
  _.times = function(n, iteratee, context) {
    var accum = Array(Math.max(0, n));
    iteratee = optimizeCb(iteratee, context, 1);
    for (var i = 0; i < n; i++) accum[i] = iteratee(i);
    return accum;
  };

  // 最小値と最大値（含む）の間のランダムな整数を返します
    if (max == null) {
      max = min;
      min = 0;
    }
    return min + Math.floor(Math.random() * (max - min + 1));
  }

  // （おそらくより速い）方法は、整数として現在のタイムスタンプを取得します。
  _.now = Date.now || function() {
    return new Date().getTime();
  };

   // (List of HTML entities for escaping.)(エスケープのためのHTMLエンティティのリスト。)
  var escapeMap = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#x27;',
    '`': '&#x60;'
  };
  var unescapeMap = _.invert(escapeMap);

  // HTML補間へ/から文字列をエスケープし、エスケープ解除のための関数。
  var createEscaper = function(map) {
    var escaper = function(match) {
      return map[match];
    };
    // 
エスケープする必要のあるキーを識別するための正規表現
    var source = '(?:' + _.keys(map).join('|') + ')';
    var testRegexp = RegExp(source);
    var replaceRegexp = RegExp(source, 'g');
    return function(string) {
      string = string == null ? '' : '' + string;
      return testRegexp.test(string) ? string.replace(replaceRegexp, escaper) : string;
    };
  };
  _.escape = createEscaper(escapeMap);
  _.unescape = createEscaper(unescapeMap);

  // 名前の `property`の値が関数であれば、とそれを呼び出します
  // `コンテキストとしてobject`。そうでなければ、それを返します。
  _.result = function(object, property, fallback) {
    var value = object == null ? void 0 : object[property];
    if (value === void 0) {
      value = fallback;
    }
    return _.isFunction(value) ? value.call(object) : value;
  };

  // （全体のクライアントセッション内で一意）一意の整数IDを生成します。
  // 仮DOMのidに便利です。
  var idCounter = 0;
  _.uniqueId = function(prefix) {
    var id = ++idCounter + '';
    return prefix ? prefix + id : id;
  };

  // デフォルトでは、アンダースコアはERB-スタイルテンプレートの区切り文字を使用して、変更します
  // 代替区切り文字を使用するテンプレートの設定を次のよう。
  _.templateSettings = {
    evaluate    : /<%([\s\S]+?)%>/g,
    interpolate : /<%=([\s\S]+?)%>/g,
    escape      : /<%-([\s\S]+?)%>/g
  };

  // `templateSettings`をカスタマイズするときは、定義したくない場合は
  // 補間、評価やエスケープ正規表現、我々は1が必要です
  // 一致しないことが保証。
  var noMatch = /(.)^/;

  // 特定の文字は、彼らがに入れることができるようにエスケープする必要があります
  // 文字列リテラル。
  var escapes = {
    "'":      "'",
    '\\':     '\\',
    '\r':     'r',
    '\n':     'n',
    '\u2028': 'u2028',
    '\u2029': 'u2029'
  };

  var escaper = /\\|'|\r|\n|\u2028|\u2029/g;

  var escapeChar = function(match) {
    return '\\' + escapes[match];
  };

  // ジョンResig氏の実装に似JavaScriptのマイクロテンプレート、。
  // テンプレートは、任意の区切り文字を扱う下線、空白が保持され、
  // そして、正しく補間されたコード内の引用符をエスケープします。
  // 注意： `oldSettings`は、後方互換性のためだけに存在します。
  _.template = function(text, settings, oldSettings) {
    if (!settings && oldSettings) settings = oldSettings;
    settings = _.defaults({}, settings, _.templateSettings);

    // 交代を経て1正規表現の中に区切り文字を組み合わせます。
    var matcher = RegExp([
      (settings.escape || noMatch).source,
      (settings.interpolate || noMatch).source,
      (settings.evaluate || noMatch).source
    ].join('|') + '|$', 'g');

    // 適切に文字列リテラルをエスケープ、テンプレートのソースをコンパイルします。
    var index = 0;
    var source = "__p+='";
    text.replace(matcher, function(match, escape, interpolate, evaluate, offset) {
      source += text.slice(index, offset).replace(escaper, escapeChar);
      index = offset + match.length;

      if (escape) {
        source += "'+\n((__t=(" + escape + "))==null?'':_.escape(__t))+\n'";
      } else if (interpolate) {
        source += "'+\n((__t=(" + interpolate + "))==null?'':__t)+\n'";
      } else if (evaluate) {
        source += "';\n" + evaluate + "\n__p+='";
      }

      // アドビVMは一致が正しいoffestを生成するために返さなければなりません。
      return match;
    });
    source += "';\n";

    // 変数が指定されていない場合は、ローカルスコープ内のデータ値を配置します。
    if (!settings.variable) source = 'with(obj||{}){\n' + source + '}\n';

    source = "var __t,__p='',__j=Array.prototype.join," +
      "print=function(){__p+=__j.call(arguments,'');};\n" +
      source + 'return __p;\n';

    try {
      var render = new Function(settings.variable || 'obj', '_', source);
    } catch (e) {
      e.source = source;
      throw e;
    }

    var template = function(data) {
      return render.call(this, data, _);
    };

    // プリコンパイルのための便宜としてコンパイルされたソースを提供します。
    var argument = settings.variable || 'obj';
    template.source = 'function(' + argument + '){\n' + source + '}';

    return template;
  };

  // 「チェーン」機能を追加します。ラップされたアンダースコアオブジェクトを連鎖開始します。
  _.chain = function(obj) {
    var instance = _(obj);
    instance._chain = true;
    return instance;
  };

  // OOP
  // ---------------
  // アンダースコアは、関数として呼び出された場合、そのラップされたオブジェクトを返します。
  // OOスタイルを使用することができます。このラッパーは、すべての変更されたバージョンを保持しています
  // 機能性を強調する。ラップされたオブジェクトを連結することが可能です

  // 中間結果を連鎖継続するヘルパー関数。
  var result = function(instance, obj) {
    return instance._chain ? _(obj).chain() : obj;
  };

  // 下線オブジェクトに独自のカスタム機能を追加します。
  _.mixin = function(obj) {
    _.each(_.functions(obj), function(name) {
      var func = _[name] = obj[name];
      _.prototype[name] = function() {
        var args = [this._wrapped];
        push.apply(args, arguments);
        return result(this, func.apply(_, args));
      };
    });
  };

  // ラッパーオブジェクトに下線機能のすべてを追加します。
  _.mixin(_);

  // ラッパーにすべてのミューテータ配列関数を追加します。
  _.each(['pop', 'push', 'reverse', 'shift', 'sort', 'splice', 'unshift'], function(name) {
    var method = ArrayProto[name];
    _.prototype[name] = function() {
      var obj = this._wrapped;
      method.apply(obj, arguments);
      if ((name === 'shift' || name === 'splice') && obj.length === 0) delete obj[0];
      return result(this, obj);
    };
  });

  // ラッパーにすべてのアクセサ配列関数を追加します。
  _.each(['concat', 'join', 'slice'], function(name) {
    var method = ArrayProto[name];
    _.prototype[name] = function() {
      return result(this, method.apply(this._wrapped, arguments));
    };
  });

  // 包み、連鎖したオブジェクトから結果を抽出します。
  _.prototype.value = function() {
    return this._wrapped;
  };

  // エンジン操作で使用されるいくつかのメソッドのプロキシをアンラップ提供
  // このような演算のJSON文字列化など。
  _.prototype.valueOf = _.prototype.toJSON = _.prototype.value;

  _.prototype.toString = function() {
    return '' + this._wrapped;
  };

  // 算術とJSON stringification.AMD登録はAMDローダーとの互換性のために最後に発生したような
  // それは、モジュール上の次のターンのセマンティクスを強制しないことがあります。でも一般的なものの
  // AMDの登録のための練習が匿名であることがあり、レジスタを強調する
  // 名前のモジュールとしてjQueryのように、それは基本ライブラリである、ので、
  // 十分な人気は、サードパーティ製のlibにバンドルされたが、の一部ではないことにします
  // AMDのロード要求。これらの例は、エラーを生成することができたとき
  // 匿名の定義は（）ローダ要求の外側で呼び出されました。
  if (typeof define === 'function' && define.amd) {
    define('underscore', [], function() {
      return _;
    });
  }
}.call(this));